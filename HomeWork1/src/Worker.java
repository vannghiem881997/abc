
public class Worker extends Person{
    private double hrs;
    private final double RATE=5.5;

    public Worker() {
    }

    public Worker(String name,double hrs) {
        super(name);
        this.hrs = hrs;
    }@Override
    public double getSalary(){
        return hrs*5.5;
    }
    @Override
    public void display(){
        System.out.println("{Name ="+name+",Salary= "+getSalary()+"}");
    }
    
    
    
}
