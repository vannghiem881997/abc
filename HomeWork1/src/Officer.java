
public class Officer extends Person{
    private double bSalary;

    public Officer() {
    }

    public Officer( String name,double bSalary) {
        super(name);
        this.bSalary = bSalary;
    }
    @Override
    public void display(){
        System.out.println( "{Name="+ name+ ",Salary="+bSalary+"}");
    }
    @Override
    public double getSalary(){
        return bSalary;
    }

    
    
    
}
